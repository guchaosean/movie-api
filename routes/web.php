<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});



Route::group(['prefix' => 'api', 'middleware' => 'throttle:50,10'], function() {

    //Authentication
    Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'AuthenticateController@authenticate');

    //Movie
    Route::get('movies', 'MovieController@get');
    Route::post('movies', 'MovieController@post');
    Route::get('movie/{movie_id}', 'MovieController@retrieve');
    Route::patch('movie/{movie_id}', 'MovieController@update');
    Route::get('movie/{movie_id}/actors', 'MovieController@getActors');
    Route::post('movie/{movie_id}/actors', 'MovieController@addActor');
    Route::get('movie/{movie_id}/images', 'MovieController@getImages');
    Route::post('movie/{movie_id}/images', 'MovieController@addImage');
    ////Favourite a movie 
    Route::get('movie/{movie_id}/favourite', 'MovieController@favourite');
    Route::delete('movie/{movie_id}/favourite', 'MovieController@unfavourite');
    

    //Actor
    Route::get('actors', 'ActorController@get');
    Route::post('actors', 'ActorController@post');
    Route::get('actor/{actor_id}', 'ActorController@retrieve');
    Route::patch('actor/{actor_id}', 'ActorController@update');

    //Genre
    Route::get('genres', 'GenreController@get');
    Route::post('genres', 'GenreController@post');
    Route::get('genre/{genre_id}/movies', 'GenreController@getMovies');
    Route::post('genre/{genre_id}/movies', 'GenreController@addMovies');
});
