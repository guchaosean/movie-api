<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

 ## Movie API Test

This application is based on Laravel.

Guidence for quick start:

- git clone https://gitlab.com/guchaosean/movie-api.git
- cd movie-api
- cp .env.example .env
- composer install
- php artisan key:generate
- create db , db name: movie-api
- php artisan migrate  (DB migration)
- php artisan db:seed  (DB seeding)


## Authentication Mechanism

Basically use Json Web Token as the Authentication Mechanism. Need to include a token in header for all the other API calls except for authentication endpoint, which is:
{BASE_URL}/api/authenticate

You can find more information about JWT at https://github.com/tymondesigns/jwt-auth

Other endpoints are secured via Middleware.

This implements point 1,2

## API Endpoints and API blueprint

API blueprint : https://movieapi63.docs.apiary.io/ 
All endpoints can be found there. Here are a few things need to be pointed out:

- The image for actor is not implemented. It should be very similar for movie's image.
- The genre for actor is not implemented. The reason is same above. 
- When saving the images, an thumbnail is also saved. See POST /api/movies/{movie_id}/images (BONUS POINT 3)
- Rating limiting is implemented with my own Throttle Class. It's in middleware (BONUS POINT 2)
- user could favourite a movie or un-favourite it. (BONUS POINT 1)

This implements point 3 ,4 ,5, 6


## Testing 
 
The initial testing during development was implemented using POSTMAN. 

I tried to implement Dredd with the API blueprint for auto testing. But I end up with all failed because the token I get doesn't match the random token in my API document. I think it can be get around with Hooks in Dredd.

This implements point 7


## Regarding Docker

I have tried to install packages from docker before. But I have not get the experience to pack my own package to docker.


 