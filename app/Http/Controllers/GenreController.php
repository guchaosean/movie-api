<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\GenreMovie;
use App\Models\Movie;

class GenreController extends Controller {

    //

    public function __construct() {
        // JWT middleware  
        $this->middleware('jwt.auth');
    }

    //get all genres
    public function get() {
        $genres = Genre::all();
        return response()->json($genres, 200);
    }

    //create a new genre
    public function post(Request $request) {

        $genre = new Genre();
        if ($genre->validate($request->all())) {
            $response = Genre::create($request->all());
            $HttpCode = '201';
        } else {
            $response['error'] = $genre->errors();
            $HttpCode = '400';
        }

        return response()->json($response, $HttpCode);
    }

    //get all movies from one genre
    public function getMovies($genre_id) {

        $genre = Genre::find($genre_id);
        if (isset($genre)) {
            $response = $genre->movies;
            $HttpCode = '200';
        } else {
            $response['error'] = 'can`t find genre with this ID! ';
            $HttpCode = '400';
        }
        return response()->json($response, $HttpCode);
    }

    //add a movie to one genre
    public function addMovies(Request $request, $genre_id) {
        $genre = Genre::find($genre_id);
        if (!isset($genre)) {
            $response['error'] = 'can`t find genre with this ID!';
            return response()->json($response, 400);
        }

        $movie = Movie::find($request->movie_id);
        if (!isset($movie)) {
            $response['error'] = 'can`t find movie with this ID!';
            return response()->json($response, 400);
        }
        $search = GenreMovie::where(['genre_id' => $genre_id, 'movie_id' => $request->movie_id])->first();
        if (isset($search)) {
            $response['error'] = 'movie already exist in this genre!';
            return response()->json($response, 400);
        }

        $response = GenreMovie::create([
                    'genre_id' => $genre_id,
                    'movie_id' => $request->movie_id,
        ]);
        $HttpCode = '201';

        return response()->json($response, $HttpCode);
    }

}
