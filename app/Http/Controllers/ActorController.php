<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Actor;

class ActorController extends Controller {

    public function __construct() {
        // JWT middleware  
        $this->middleware('jwt.auth');
    }

    //Get all actors
    public function get() {

        $actors = Actor::all();
        return response()->json($actors, 200);
    }

    //Create a new Actor 
    public function post(Request $request) {

        $actor = new Actor();
        if ($actor->validate($request->all())) {
            $response = Actor::createRecord($request->all());
            $HttpCode = '201';
        } else {
            $response['error'] = $actor->errors();
            $HttpCode = '400';
        }

        return response()->json($response, $HttpCode);
    }

    public function retrieve(Request $request, $actor_id) {

        $actor = Actor::find($actor_id);
        if (isset($actor)) {
            $response = $actor;
            $HttpCode = '200';
        } else {
            $response['error'] = 'can`t find actor with this ID! ';
            $HttpCode = '400';
        }

        return response()->json($response, $HttpCode);
    }

    public function update(Request $request, $actor_id) {

        $actor = Actor::find($actor_id);
        if (isset($actor)) {
            if ($actor->validatePatch($request->all())) {
                $actor->updateRecord($request->all());
                $response = $actor;
                $HttpCode = '200';
            } else {
                $response['error'] = $actor->errors();
                $HttpCode = '400';
            }
        } else {
            $response['error'] = 'can`t find actor with this ID! ';
            $HttpCode = '400';
        }

        return response()->json($response, 200);
    }

}
