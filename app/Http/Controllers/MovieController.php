<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Movie;
use App\Models\MovieActor;
use App\Models\Actor;
use App\Models\MovieImage;
use Intervention\Image\Facades\Image;
use App\Models\UserFavourite;

class MovieController extends Controller {

    public function __construct() {
        // JWT middleware  
        $this->middleware('jwt.auth');
    }

    //List all movie
    public function get() {

        $movies = Movie::all();

        return response()->json($movies, 200);
    }

    //Create a movie
    public function post(Request $request) {

        $movie = new Movie();

        if ($movie->validate($request->all())) {
            $response = Movie::create($request->all());
            $HttpCode = '201';
        } else {
            $response['error'] = $movie->errors();
            $HttpCode = '400';
        }

        return response()->json($response, $HttpCode);
    }

    //Retrieve single movie by id
    public function retrieve($movie_id) {

        $movie = Movie::find($movie_id);
        if (isset($movie)) {
            $response = $movie;
            $HttpCode = '200';
        } else {
            $response['error'] = 'can`t find movie with this ID! ';
            $HttpCode = '400';
        }

        return response()->json($response, $HttpCode);
    }

    //update single movie by id
    public function update(Request $request, $movie_id) {

        $movie = Movie::find($movie_id);
        if (isset($movie)) {
            if ($movie->validatePatch($request->all())) {
                $movie->update($request->all());
                $response = $movie;
                $HttpCode = '200';
            } else {
                $response['error'] = $movie->errors();
                $HttpCode = '400';
            }
        } else {
            $response['error'] = 'can`t find movie with this ID! ';
            $HttpCode = '400';
        }

        return response()->json($response, 200);
    }

    //get all Actors from one single movie
    public function getActors($movie_id) {
        $movie = Movie::find($movie_id);
        if (isset($movie)) {
            $response = $movie->actors;
            $HttpCode = '200';
        } else {
            $response['error'] = 'can`t find movie with this ID! ';
            $HttpCode = '400';
        }
        return response()->json($response, $HttpCode);
    }

    //add  Actor to one single movie
    public function addActor(Request $request, $movie_id) {
        $movie = Movie::find($movie_id);
        if (!isset($movie)) {
            $response['error'] = 'can`t find movie with this ID!';
            return response()->json($response, 400);
        }
        $actorMovie = new MovieActor();
        if ($actorMovie->validate($request->all())) {
            $actor = Actor::find($request->actor_id);
            if (!isset($actor)) {
                $response['error'] = 'can`t find movie with this ID!';
                return response()->json($response, 400);
            }
            $search = MovieActor::where(['movie_id' => $movie_id, 'actor_id' => $request->actor_id])->first();
            if (isset($search)) {
                $response['error'] = 'actor already exist in this movie!';
                return response()->json($response, 400);
            }
            $response = MovieActor::create([
                        'movie_id' => $movie_id,
                        'actor_id' => $request->actor_id,
                        'character_name' => $request->character_name,
            ]);
            $HttpCode = '201';
        } else {
            $response['error'] = $actorMovie->errors();
            $HttpCode = '400';
        }
        return response()->json($response, $HttpCode);
    }

    //get images of a movie
    public function getImages($movie_id) {

        $movie = Movie::find($movie_id);
        if (isset($movie)) {
            $response = $movie->images;
            $HttpCode = '200';
        } else {
            $response['error'] = 'can`t find movie with this ID! ';
            $HttpCode = '400';
        }
        return response()->json($response, $HttpCode);
    }

    //add image to a movie
    public function addImage(Request $request, $movie_id) {
        $movie = Movie::find($movie_id);
        if (!isset($movie)) {
            $response['error'] = 'can`t find movie with this ID!';
            return response()->json($response, 400);
        }
        $movieImage = new MovieImage();
        if ($request->image) {
            $fileFillName = 'FullSize';
            $fileThumbnailName = 'Thumbnail';
            $pathFolder = public_path('movie-images/' . 'movie' . '-' . substr(md5($movie_id . '-' . time()), 0, 15) . '/');
            if (!file_exists($pathFolder)) {
                mkdir($pathFolder, 666, true);
            }

            //Create both full size and thumbnail size image
            Image::make($request->image)->orientate()->save($pathFolder . $fileFillName);
            Image::make($request->image)->orientate()->fit(200, 100)->save($pathFolder . $fileThumbnailName);

            $movieImage->path_full = $pathFolder . $fileFillName;
            $movieImage->path_thumbnail = $pathFolder . $fileThumbnailName;
            $movieImage->movie_id = $movie_id;
            $movieImage->save();
            $response = $movieImage;
            $HttpCode = '201';
        }
        return response()->json($response, $HttpCode);
    }

    //favourite a movie for current user
    public function favourite($movie_id) {
        $user = Auth::user()->id;

        $search = UserFavourite::where([
                    'movie_id' => $movie_id,
                    'user_id' => $user
                ])->first();
        if (isset($search)) {
            $response['error'] = 'movie already favourited by this user!';
            return response()->json($response, 400);
        }

        UserFavourite::create([
            'movie_id' => $movie_id,
            'user_id' => $user
        ]);

        $response['response'] = 'movie favourited!';
        return response()->json($response, 201);
    }

    //favourite a movie for current user
    public function unfavourite($movie_id) {
        $user = Auth::user()->id;

        $search = UserFavourite::where([
                    'movie_id' => $movie_id,
                    'user_id' => $user
                ])->first();
        if (!isset($search)) {
            $response['error'] = 'movie didn`t favourited by this user before!';
            return response()->json($response, 400);
        }

        $search->delete();

        $response['response'] = 'movie unfavourited!';
        return response()->json($response, 200);
    }

}
