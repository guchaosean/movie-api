<?php

namespace App\Models;

use App\Models\Elegant;

class MovieActor extends Elegant {

    //
    protected $table = 'actor_movie';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * My Customized Validation rules
     *  
     * 
     * @var array
     */
    protected $rules = array(
        'actor_id' => 'required|integer',
        'character_name' => 'required'
    );
    protected $fillable = ['movie_id', 'actor_id', 'character_name'];

}
