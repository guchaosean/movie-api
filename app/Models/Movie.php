<?php

namespace App\Models;

use App\Models\Elegant;

class Movie extends Elegant {

    protected $table = 'movie';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * My Customized Validation rules
     *  
     * 
     * @var array
     */
    protected $rules = array(
        'name' => 'required',
        'rating' => 'integer|min:0|max:100'
    );
    protected $fillable = ['name', 'rating', 'description'];

    public function actors() {
        return $this->belongsToMany('App\Models\Actor')->withPivot('character_name');
    }

    public function images() {
        return $this->hasMany('App\Models\MovieImage');
    }

}
