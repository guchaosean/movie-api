<?php

namespace App\Models;

use App\Models\Elegant;

class GenreMovie extends Elegant {

    //
    protected $table = 'genre_movie';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * My Customized Validation rules
     *  
     * 
     * @var array
     */
    protected $fillable = ['movie_id', 'genre_id'];

}
