<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

/* My little Help Class for Model Validation
 * All other Models extend from this Class
 */

class Elegant extends Model {

    protected $rules = array();
    protected $errors;

    public function validate($data) {
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->messages();
            return false;
        }

        // validation pass
        return true;
    }

    /* Validate for Update , Mainly for patch 
     * Remove rules if the field is not included in the Request
     */

    public function validatePatch($data) {
        // make a new validator object
        $rules = [];
        foreach ($data as $attribute => $value) {
            if (isset($this->rules[$attribute])) {
                $rules[$attribute] = $this->rules[$attribute];
            }
        }

        $v = Validator::make($data, $rules);
        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->messages();
            return false;
        }

        // validation pass

        return true;
    }

    public function errors() {
        return $this->errors;
    }

}
