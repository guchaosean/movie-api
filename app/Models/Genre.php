<?php

namespace App\Models;

use App\Models\Elegant;

class Genre extends Elegant {

    //
    protected $table = 'genre';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * My Customized Validation rules
     *  
     * 
     * @var array
     */
    protected $rules = array(
        'name' => 'required',
    );
    protected $fillable = ['name'];

    public function movies() {
        return $this->belongsToMany('App\Models\Movie');
    }

}
