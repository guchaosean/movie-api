<?php

namespace App\Models;

use App\Models\Elegant;

class UserFavourite extends Elegant {

    //
    protected $table = 'users_favourite';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = ['movie_id', 'user_id'];

}
