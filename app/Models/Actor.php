<?php

namespace App\Models;

use App\Models\Elegant;

class Actor extends Elegant {

    //
    protected $table = 'actor';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * My Customized Validation rules
     *  
     * 
     * @var array
     */
    protected $rules = array(
        'name' => 'required',
        'dob' => 'date_format:Y-m-d',
        'age' => 'integer',
    );
    protected $fillable = ['name', 'dob', 'age', 'bio'];

    public function movies() {
        return $this->belongsToMany('App\Models\Movie');
    }

    //get age from DOB
    private function getAge($dob) {
        $age = \DateTime::createFromFormat('Y-m-d', $dob)
                        ->diff(new \DateTime('now'))
                ->y;
        return $age;
    }

    //Cal Age based on DOB on save
    public static function createRecord($data) {

        $actor = Actor::create($data);
        $actor->age = $this->getAge($data['dob']);
        $actor->save();
        return $actor;
    }

    //Cal Age on update
    public function updateRecord($data) {

        $this->update($data);
        $this->age = $this->getAge($data['dob']);
        $this->save();
    }

}
