<?php

namespace App\Models;

use App\Models\Elegant;

class MovieImage extends Elegant {

    protected $table = 'image_movie';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * My Customized Validation rules
     *  
     * 
     * @var array
     */
    protected $rules = array(
        'movie_id' => 'required|integer',
        'path_full' => 'required|string',
        'path_thumbnail' => 'required|string'
    );
    protected $fillable = ['movie_id', 'path_full', 'path_thumbnail'];

}
