<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenreActorTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('genre-actor', function (Blueprint $table) {
            //DB setting
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            //Table Field setting
            $table->increments('id');
            $table->integer('actor_id')->unsigned();
            $table->integer('genre_id')->unsigned();
            $table->timestamps();

            //Forign Key Setting
            //N TO N relationship
            $table->index('actor_id');
            $table->foreign('actor_id')->references('id')->on('actor')->onDelete('cascade');
            $table->index('genre_id');
            $table->foreign('genre_id')->references('id')->on('genre')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
