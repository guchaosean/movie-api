<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovieActorTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('actor_movie', function (Blueprint $table) {
            //DB setting
            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            //Table Field setting
            $table->increments('id');
            $table->integer('movie_id')->unsigned();
            $table->integer('actor_id')->unsigned();
            $table->string('character_name')->nullable();
            $table->timestamps();

            //Forign Key Setting
            //N TO N relationship
            $table->index('movie_id');
            $table->foreign('movie_id')->references('id')->on('movie')->onDelete('cascade');
            $table->index('actor_id');
            $table->foreign('actor_id')->references('id')->on('actor')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
