<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GenreMovieSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('genre_movie')->insert([
            'movie_id' => 2,
            'genre_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('genre_movie')->insert([
            'movie_id' => 1,
            'genre_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

}
