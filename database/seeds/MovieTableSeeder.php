<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MovieTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //

        DB::table('movie')->insert([
            'name' => 'The Fate of the Furious',
            'rating' => 89,
            'description' => 'The Fate of the Furious (alternatively known as Fast & Furious 8 and Fast 8, and often stylized as F8) is a 2017 American action film directed by F. Gary Gray and written by Chris Morgan.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        
         DB::table('movie')->insert([
            'name' => 'Maze Runner: The Death Cure',
            'rating' => 70,
            'description' => 'Thomas leads some escaped Gladers on their final and most dangerous mission yet.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

}
