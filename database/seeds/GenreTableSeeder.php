<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GenreTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('genre')->insert([
            'name' => 'Action',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

}
