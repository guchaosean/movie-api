<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ActorTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('actor')->insert([
            'name' => 'Vin Diesel',
            'dob' => '1967-07-18',
            'age' => 50,
            'bio' => str_random(1000),
            'image' => '[]',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('actor')->insert([
            'name' => 'Dwayne Johnson',
            'dob' => '1972-05-02',
            'age' => 45,
            'bio' => str_random(1000),
            'image' => '[]',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

}
