<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MovieActorSeed extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('actor_movie')->insert([
            'movie_id' => 1,
            'actor_id' => 1,
            'character_name' => 'Dominic Dom Toretto',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('actor_movie')->insert([
            'movie_id' => 1,
            'actor_id' => 2,
            'character_name' =>  'Luke Hobbs',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }

}
